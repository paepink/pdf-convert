package allpdf.readerconverter.interfaces;

public interface ItemSelectedListener {
    void isSelected(Boolean isSelected, int countFiles);
}