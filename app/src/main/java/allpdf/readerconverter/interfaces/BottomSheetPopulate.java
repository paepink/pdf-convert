package allpdf.readerconverter.interfaces;

import java.util.ArrayList;

public interface BottomSheetPopulate {
    void onPopulate(ArrayList<String> paths);
}
