package allpdf.readerconverter.interfaces;

public interface DataSetChanged {
    void updateDataset();
}
