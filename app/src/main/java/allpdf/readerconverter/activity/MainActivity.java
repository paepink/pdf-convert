package allpdf.readerconverter.activity;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import java.util.ArrayList;
import java.util.Objects;

import allpdf.readerconverter.BuildConfig;
import allpdf.readerconverter.R;
import allpdf.readerconverter.fragment.AddImagesFragment;
import allpdf.readerconverter.fragment.FavouritesFragment;
import allpdf.readerconverter.fragment.HistoryFragment;
import allpdf.readerconverter.fragment.HomeFragment;
import allpdf.readerconverter.fragment.ImageToPdfFragment;
import allpdf.readerconverter.fragment.InvertPdfFragment;
import allpdf.readerconverter.fragment.MergeFilesFragment;
import allpdf.readerconverter.fragment.PdfToImageFragment;
import allpdf.readerconverter.fragment.RemoveDuplicatePagesFragment;
import allpdf.readerconverter.fragment.RemovePagesFragment;
import allpdf.readerconverter.fragment.SettingsFragment;
import allpdf.readerconverter.fragment.SplitFilesFragment;
import allpdf.readerconverter.fragment.TextToPdfFragment;
import allpdf.readerconverter.fragment.ViewFilesFragment;
import allpdf.readerconverter.util.FeedbackUtils;
import allpdf.readerconverter.util.ThemeUtils;

import static allpdf.readerconverter.util.Constants.ACTION_MERGE_PDF;
import static allpdf.readerconverter.util.Constants.ACTION_SELECT_IMAGES;
import static allpdf.readerconverter.util.Constants.ACTION_TEXT_TO_PDF;
import static allpdf.readerconverter.util.Constants.ACTION_VIEW_FILES;
import static allpdf.readerconverter.util.Constants.ADD_IMAGES;
import static allpdf.readerconverter.util.Constants.ADD_PWD;
import static allpdf.readerconverter.util.Constants.BUNDLE_DATA;
import static allpdf.readerconverter.util.Constants.COMPRESS_PDF;
import static allpdf.readerconverter.util.Constants.EXTRACT_IMAGES;
import static allpdf.readerconverter.util.Constants.LAUNCH_COUNT;
import static allpdf.readerconverter.util.Constants.OPEN_SELECT_IMAGES;
import static allpdf.readerconverter.util.Constants.PDF_TO_IMAGES;
import static allpdf.readerconverter.util.Constants.REMOVE_PAGES;
import static allpdf.readerconverter.util.Constants.REMOVE_PWd;
import static allpdf.readerconverter.util.Constants.REORDER_PAGES;
import static allpdf.readerconverter.util.Constants.VERSION_NAME;
import static allpdf.readerconverter.util.DialogUtils.ADD_WATERMARK;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FeedbackUtils mFeedbackUtils;
    private NavigationView mNavigationView;
    private SharedPreferences mSharedPreferences;
    private boolean mDoubleBackToExitPressedOnce = false;
    private InterstitialAd mInterstitialAd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ThemeUtils.setThemeApp(this);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        //show Banner Add
        AdView mAdView =findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("C878CA227D8D61A65BB1C07664F74E9B").build();
        mAdView.loadAd(adRequest);
        //show mInterstitialAd
        MobileAds.initialize(this, getString(R.string.app_id));
        mInterstitialAd= new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice("C878CA227D8D61A65BB1C07664F74E9B").build());
        mInterstitialAd.setAdListener(new AdListener(){
            public void onAdFailedToLoad(int var1) {
                System.out.println("MainActivity onAdFailedToLoad:" + var1);
            }

            public void onAdLoaded() {
                System.out.println("MainActivity onAdLoaded:" );
            }
        });



        Toolbar toolbar = findViewById(R.id.toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        setSupportActionBar(toolbar);

        // Set navigation drawer
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.app_name, R.string.app_name);

        //Replaced setDrawerListener with addDrawerListener because it was deprecated.
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        // initialize values
        initializeValues();

        // suitable xml parsers for reading .docx files
        System.setProperty("org.apache.poi.javax.xml.stream.XMLInputFactory",
                "com.fasterxml.aalto.stax.InputFactoryImpl");
        System.setProperty("org.apache.poi.javax.xml.stream.XMLOutputFactory",
                "com.fasterxml.aalto.stax.OutputFactoryImpl");
        System.setProperty("org.apache.poi.javax.xml.stream.XMLEventFactory",
                "com.fasterxml.aalto.stax.EventFactoryImpl");

        // Check for app shortcuts & select default fragment
        Fragment fragment = checkForAppShortcutClicked();

        // Check if  images are received
        handleReceivedImagesIntent(fragment);

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        int count = mSharedPreferences.getInt(LAUNCH_COUNT, 0);
        if (count > 0 && count % 15 == 0)
            mFeedbackUtils.rateUs();
        mSharedPreferences.edit().putInt(LAUNCH_COUNT, count + 1).apply();

        String versionName = mSharedPreferences.getString(VERSION_NAME, "");
        if (!versionName.equals(BuildConfig.VERSION_NAME)) {
            mSharedPreferences.edit().putString(VERSION_NAME, BuildConfig.VERSION_NAME).apply();
        }
        getRuntimePermissions();

        //check for welcome activity

    }

    public void adShow() {
        mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice("C878CA227D8D61A65BB1C07664F74E9B").build());
    }

    public void onAddLodded() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice("C878CA227D8D61A65BB1C07664F74E9B").build());
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        ActionBar actionBar = getSupportActionBar();
        actionBar.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_favourites, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_favourites_item) {
            Fragment fragment = new FavouritesFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content, fragment).commit();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Sets a fragment based on app shortcut selected, otherwise default
     *
     * @return - instance of current fragment
     */
    private Fragment checkForAppShortcutClicked() {
        Fragment fragment = new HomeFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();

        if (getIntent().getAction() != null) {
            switch (Objects.requireNonNull(getIntent().getAction())) {
                case ACTION_SELECT_IMAGES:
                    fragment = new ImageToPdfFragment();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(OPEN_SELECT_IMAGES, true);
                    fragment.setArguments(bundle);
                    break;
                case ACTION_VIEW_FILES:
                    fragment = new ViewFilesFragment();
                    setDefaultMenuSelected(1);
                    break;
                case ACTION_TEXT_TO_PDF:
                    fragment = new TextToPdfFragment();
                    setDefaultMenuSelected(4);
                    break;
                case ACTION_MERGE_PDF:
                    fragment = new MergeFilesFragment();
                    setDefaultMenuSelected(2);
                    break;
                default:
                    // Set default fragment
                    fragment = new HomeFragment();
                    break;
            }
        }
        if (areImagesRecevied())
            fragment = new ImageToPdfFragment();

        fragmentManager.beginTransaction().replace(R.id.content, fragment).commit();

        return fragment;
    }


    /**
     * Ininitializes default values
     */
    private void initializeValues() {
        mFeedbackUtils = new FeedbackUtils(this);
        mNavigationView = findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);
        setDefaultMenuSelected(0);
    }

    /*
     * This will set default menu item selected at the position mentioned
     */
    public void setDefaultMenuSelected(int position) {
        if (mNavigationView != null && mNavigationView.getMenu() != null &&
                position < mNavigationView.getMenu().size()
                && mNavigationView.getMenu().getItem(position) != null) {
            mNavigationView.getMenu().getItem(position).setChecked(true);
        }
    }

    /**
     * Checks if images are received in the intent
     *
     * @param fragment - instance of current fragment
     */
    private void handleReceivedImagesIntent(Fragment fragment) {
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (type == null || !type.startsWith("image/"))
            return;

        if (Intent.ACTION_SEND_MULTIPLE.equals(action)) {
            handleSendMultipleImages(intent, fragment); // Handle multiple images
        } else if (Intent.ACTION_SEND.equals(action)) {
            handleSendImage(intent, fragment); // Handle single image
        }
    }


    private boolean areImagesRecevied() {
        Intent intent = getIntent();
        String type = intent.getType();
        return type != null && type.startsWith("image/");
    }

    /**
     * Get image uri from intent and send the image to homeFragment
     *
     * @param intent   - intent containing image uris
     * @param fragment - instance of homeFragment
     */
    private void handleSendImage(Intent intent, Fragment fragment) {
        Uri uri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
        ArrayList<Uri> imageUris = new ArrayList<>();
        imageUris.add(uri);
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(getString(R.string.bundleKey), imageUris);
        fragment.setArguments(bundle);
    }

    /**
     * Get ArrayList of image uris from intent and send the image to homeFragment
     *
     * @param intent   - intent containing image uris
     * @param fragment - instance of homeFragment
     */
    private void handleSendMultipleImages(Intent intent, Fragment fragment) {
        ArrayList<Uri> imageUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        if (imageUris != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(getString(R.string.bundleKey), imageUris);
            fragment.setArguments(bundle);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Fragment currentFragment = getSupportFragmentManager()
                    .findFragmentById(R.id.content);
            if (currentFragment instanceof HomeFragment) {
                checkDoubleBackPress();
            } else {
                Fragment fragment = new HomeFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.content, fragment).commit();
                setDefaultMenuSelected(0);
            }
        }
    }

    /**
     * Closes the app only when double clicked
     */
    private void checkDoubleBackPress() {
        if (mDoubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.mDoubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.confirm_exit_message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        Fragment fragment = null;
        FragmentManager fragmentManager = getSupportFragmentManager();
        Bundle bundle = new Bundle();

        switch (item.getItemId()) {
            case R.id.nav_home:
                fragment = new HomeFragment();
                onAddLodded();
                adShow();
                break;
            case R.id.nav_camera:
                fragment = new ImageToPdfFragment();
                onAddLodded();
                adShow();
                break;
            case R.id.nav_gallery:
                fragment = new ViewFilesFragment();
                onAddLodded();
                adShow();
                break;
            case R.id.nav_merge:
                fragment = new MergeFilesFragment();
                onAddLodded();
                adShow();
                break;
            case R.id.nav_split:
                fragment = new SplitFilesFragment();
                onAddLodded();
                adShow();
                break;
            case R.id.nav_text_to_pdf:
                fragment = new TextToPdfFragment();
                onAddLodded();
                adShow();
                break;
            case R.id.nav_history:
                fragment = new HistoryFragment();
                onAddLodded();
                adShow();
                break;
            case R.id.nav_add_password:
                fragment = new RemovePagesFragment();
                bundle.putString(BUNDLE_DATA, ADD_PWD);
                fragment.setArguments(bundle);
                onAddLodded();
                adShow();
                break;
            case R.id.nav_remove_password:
                fragment = new RemovePagesFragment();
                bundle.putString(BUNDLE_DATA, REMOVE_PWd);
                fragment.setArguments(bundle);
                onAddLodded();
                adShow();
                break;
            case R.id.nav_settings:
                fragment = new SettingsFragment();
                onAddLodded();
                adShow();
                break;
            case R.id.nav_extract_images:
                fragment = new PdfToImageFragment();
                bundle.putString(BUNDLE_DATA, EXTRACT_IMAGES);
                fragment.setArguments(bundle);
                onAddLodded();
                adShow();
                break;
            case R.id.nav_pdf_to_images:
                fragment = new PdfToImageFragment();
                bundle.putString(BUNDLE_DATA, PDF_TO_IMAGES);
                fragment.setArguments(bundle);
                onAddLodded();
                adShow();
                break;
            case R.id.nav_remove_pages:
                fragment = new RemovePagesFragment();
                bundle.putString(BUNDLE_DATA, REMOVE_PAGES);
                fragment.setArguments(bundle);
                onAddLodded();
                adShow();
                break;
            case R.id.nav_rearrange_pages:
                fragment = new RemovePagesFragment();
                bundle.putString(BUNDLE_DATA, REORDER_PAGES);
                fragment.setArguments(bundle);
                onAddLodded();
                adShow();
                break;
            case R.id.nav_compress_pdf:
                fragment = new RemovePagesFragment();
                bundle.putString(BUNDLE_DATA, COMPRESS_PDF);
                fragment.setArguments(bundle);
                onAddLodded();
                adShow();
                break;
            case R.id.nav_add_images:
                fragment = new AddImagesFragment();
                bundle.putString(BUNDLE_DATA, ADD_IMAGES);
                fragment.setArguments(bundle);
                onAddLodded();
                adShow();
                break;
            case R.id.nav_remove_duplicate_pages:
                fragment = new RemoveDuplicatePagesFragment();
                onAddLodded();
                adShow();
                break;
            case R.id.nav_invert_pdf:
                fragment = new InvertPdfFragment();
                onAddLodded();
                adShow();
                break;

            case R.id.nav_add_watermark:
                fragment = new ViewFilesFragment();
                bundle.putInt(BUNDLE_DATA, ADD_WATERMARK);
                fragment.setArguments(bundle);
                onAddLodded();
                adShow();
                break;
        }

        try {
            if (fragment != null)
                fragmentManager.beginTransaction().replace(R.id.content, fragment).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public void setNavigationViewSelection(int index) {
        mNavigationView.getMenu().getItem(index).setChecked(true);
    }

    private boolean getRuntimePermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if ((ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) ||
                    (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) ||
                    (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED)) {
                requestPermissions(new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA},
                        0);
                return false;
            }
        }
        return true;
    }
}